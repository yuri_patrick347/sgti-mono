package com.mon.sgti.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mon.sgti.dao.ProdutoDao;
import com.mon.sgti.model.ProdutoModel;

@WebServlet("/ProdutoService")
public class ProdutoService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String PROD_ID = "id_Prod";

	private static final String PROD_DESC = "desc_Prod";

	private static final String PROD_QNT = "qnt_Prod";

	private static final String PROD_REF = "ref_Prod";

	private static final String PROD_MOD = "mod_Prod";

	private static final String PROD_FORN = "forn_Prod";

	private static final String PROD_DAT_COMP = "dat_Comp_Prod";

	ProdutoDao dao = new ProdutoDao();
	ProdutoModel produto = new ProdutoModel();

	public void adiciona(HttpServletRequest request) throws ServletException, IOException {
		try {

			String descricao = request.getParameter(PROD_DESC);
			String quantidade = request.getParameter(PROD_QNT);
			String referencia = request.getParameter(PROD_REF);
			String modelo = request.getParameter(PROD_MOD);
			String fornecedor = request.getParameter(PROD_FORN);
			String dataCompra = request.getParameter(PROD_DAT_COMP);

			produto.setDescricao(descricao);
			produto.setQuantidade(Integer.parseInt(quantidade));
			produto.setReferencia(Integer.parseInt(referencia));
			produto.setModelo(modelo);
			produto.setFornecedor(fornecedor);
			produto.setDataCompra(dataCompra);

			dao.adiciona(produto);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void atualiza(HttpServletRequest request)
			throws ServletException, IOException {
		try {

			String id = request.getParameter(PROD_ID);
			String descricao = request.getParameter(PROD_DESC);
			String quantidade = request.getParameter(PROD_QNT);
			String referencia = request.getParameter(PROD_REF);
			String modelo = request.getParameter(PROD_MOD);
			String fornecedor = request.getParameter(PROD_FORN);
			String dataCompra = request.getParameter(PROD_DAT_COMP);

			produto.setId(Integer.parseInt(id));
			produto.setDescricao(descricao);
			produto.setQuantidade(Integer.parseInt(quantidade));
			produto.setReferencia(Integer.parseInt(referencia));
			produto.setModelo(modelo);
			produto.setFornecedor(fornecedor);
			produto.setDataCompra(dataCompra);

			new ProdutoDao().atualizar(produto);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void exclui(HttpServletRequest request) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			ProdutoModel produto = new ProdutoModel();
			produto.setId(Integer.parseInt(id));
			new ProdutoDao().excluir(produto);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void getAll(HttpServletRequest request) throws ServletException, IOException {
		try {
			List<ProdutoModel> produtos = new ProdutoDao().getAll();
			request.setAttribute("produtos", produtos);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void getProdutoById(HttpServletRequest request) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			produto = new ProdutoDao().getCodigo(Integer.parseInt(id));
			request.setAttribute("produto", produto);
			// request.setAttribute("produtoId", Integer.parseInt(id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void exportExcel(HttpServletResponse response) throws Exception {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=relatorioProdutos.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Produtos");
		List<ProdutoModel> produtos = new ProdutoDao().getAll();

		sheet.setDefaultColumnWidth(20);
		sheet.setDefaultRowHeight((short) 400);
		sheet.setHorizontallyCenter(true);

		int rowNo = 0;
		Row row = sheet.createRow(rowNo++);
		int cellnum = 0;

		Cell cell = row.createCell(cellnum);
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("RELATORIO_PRODUTOS");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Descri��o Produto");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Quantidade Produto");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Referencia Produto");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Modelo Produto");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Fornecedor Produto");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Data da Compra Produto");

		for (ProdutoModel p : produtos) {
			cellnum = 0;
			row = sheet.createRow(rowNo++);

			cell = row.createCell(cellnum++);
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(p.getDescricao());

			cell = row.createCell(cellnum++);
			cell.setCellValue(p.getQuantidade());

			cell = row.createCell(cellnum++);
			cell.setCellValue(p.getQuantidade());

			cell = row.createCell(cellnum++);
			cell.setCellValue(p.getModelo());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(p.getFornecedor());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(p.getDataCompra());
		}

		wb.write(response.getOutputStream());
		wb.close();

	}
}