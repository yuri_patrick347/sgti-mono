package com.mon.sgti.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import com.mon.sgti.dao.UsuarioDao;
import com.mon.sgti.model.UsuarioModel;

@WebServlet("/UsuarioService")
public class UsuarioService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String USER_ID = "id_User";

	private static final String USER_NOME = "nome_User";

	private static final String USER_EMAIL = "email_User";
	
	private static final String USER_LOGIN = "login_User";

	private static final String USER_SENHA = "senha_User";

	UsuarioDao dao = new UsuarioDao();
	UsuarioModel usuario = new UsuarioModel();

	public void adiciona(HttpServletRequest request) throws ServletException, IOException {
		try {

			String nome = request.getParameter(USER_NOME);
			String email = request.getParameter(USER_EMAIL);
			String login = request.getParameter(USER_LOGIN);
			String senha = request.getParameter(USER_SENHA);
			
			usuario.setNom_Usuario(nome);
			usuario.setEmail(email);
			usuario.setUsuario(login);
			usuario.setSenha(senha);
			
			dao.adiciona(usuario);


		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}
	
	public void atualiza(HttpServletRequest request)
			throws ServletException, IOException {
		try {

			String id = request.getParameter(USER_ID);
			String nome = request.getParameter(USER_NOME);
			String email = request.getParameter(USER_EMAIL);
			String login = request.getParameter(USER_LOGIN);
			String senha = request.getParameter(USER_SENHA);

			usuario.setId(Integer.parseInt(id));
			usuario.setNom_Usuario(nome);
			usuario.setEmail(email);
			usuario.setUsuario(login);
			usuario.setSenha(senha);
			

			new UsuarioDao().atualizar(usuario);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void exclui(HttpServletRequest request) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			UsuarioModel usuario = new UsuarioModel();
			usuario.setId(Integer.parseInt(id));
			new UsuarioDao().excluir(usuario);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void getAll(HttpServletRequest request) throws ServletException, IOException {
		try {
			List<UsuarioModel> usuarios = new UsuarioDao().getAll();
			request.setAttribute("usuarios", usuarios);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void getUsuarioById(HttpServletRequest request) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			usuario = new UsuarioDao().getCodigo(Integer.parseInt(id));
			request.setAttribute("usuario", usuario);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	
}