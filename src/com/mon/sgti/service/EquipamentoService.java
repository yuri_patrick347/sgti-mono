package com.mon.sgti.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mon.sgti.dao.EquipamentoDao;
import com.mon.sgti.model.EquipamentoModel;

@WebServlet("/EquipamentoService")
public class EquipamentoService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String EQUIP_ID = "id_Equip";

	private static final String EQUIP_TIPO = "tipo_Equip";

	private static final String EQUIP_SO = "sistema_Equip";

	private static final String EQUIP_ARQUITETURA = "arq_Equip";
	
	private static final String EQUIP_PROC = "processador_Equip";

	private static final String EQUIP_MEM = "memoria_Equip";

	private static final String EQUIP_MARCA = "marca_Equip";

	private static final String EQUIP_FILIAL = "filial_Equip";
	
	private static final String EQUIP_SETOR = "setor_Equip";
	
	private static final String EQUIP_NOME_COMPUT = "nom_comp_Equip";
	
	private static final String EQUIP_USU_RESP = "usua_resp_Equip";
	
	EquipamentoDao dao = new EquipamentoDao();
	EquipamentoModel equipamento = new EquipamentoModel();

	public void adiciona(HttpServletRequest request) throws ServletException, IOException {
		try {
			
			String tipoEquipamento = request.getParameter(EQUIP_TIPO);
			String sistemaOperacional = request.getParameter(EQUIP_SO);
			String arquitetura = request.getParameter(EQUIP_ARQUITETURA);
			String processador = request.getParameter(EQUIP_PROC);
			String memoria = request.getParameter(EQUIP_MEM);
			String marca = request.getParameter(EQUIP_MARCA);
			String filial = request.getParameter(EQUIP_FILIAL);
			String setor = request.getParameter(EQUIP_SETOR);
			String nomeComp = request.getParameter(EQUIP_NOME_COMPUT);
			String usuarioResp = request.getParameter(EQUIP_USU_RESP);

			equipamento.setTipoEquipamento(tipoEquipamento);
			equipamento.setSistemaOperacional(sistemaOperacional);
			equipamento.setArquitetura(Integer.parseInt(arquitetura));
			equipamento.setProcessador(processador);
			equipamento.setMemoria(Integer.parseInt(memoria));
			equipamento.setMarca(marca);
			equipamento.setFilial(filial);
			equipamento.setSetor(setor);
			equipamento.setNomeComp(nomeComp);
			equipamento.setUsuarioResp(usuarioResp);

			dao.adiciona(equipamento);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void atualiza(HttpServletRequest request)
			throws ServletException, IOException {
		try {
			
			String idEquipamento = request.getParameter(EQUIP_ID);
			String tipoEquipamento = request.getParameter(EQUIP_TIPO);
			String sistemaOperacional = request.getParameter(EQUIP_SO);
			String arquitetura = request.getParameter(EQUIP_ARQUITETURA);
			String processador = request.getParameter(EQUIP_PROC);
			String memoria = request.getParameter(EQUIP_MEM);
			String marca = request.getParameter(EQUIP_MARCA);
			String filial = request.getParameter(EQUIP_FILIAL);
			String setor = request.getParameter(EQUIP_SETOR);
			String nomeComp = request.getParameter(EQUIP_NOME_COMPUT);
			String usuarioResp = request.getParameter(EQUIP_USU_RESP);

			equipamento.setId(Integer.parseInt(idEquipamento));
			equipamento.setTipoEquipamento(tipoEquipamento);
			equipamento.setSistemaOperacional(sistemaOperacional);
			equipamento.setArquitetura(Integer.parseInt(arquitetura));
			equipamento.setProcessador(processador);
			equipamento.setMemoria(Integer.parseInt(memoria));
			equipamento.setMarca(marca);
			equipamento.setFilial(filial);
			equipamento.setSetor(setor);
			equipamento.setNomeComp(nomeComp);
			equipamento.setUsuarioResp(usuarioResp);

			new EquipamentoDao().atualizar(equipamento);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void exclui(HttpServletRequest request) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			EquipamentoModel equipamento = new EquipamentoModel();
			equipamento.setId(Integer.parseInt(id));
			new EquipamentoDao().excluir(equipamento);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void getAll(HttpServletRequest request) throws ServletException, IOException {
		try {
			List<EquipamentoModel> equipamentos = new EquipamentoDao().getAll();
			request.setAttribute("equipamentos", equipamentos);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void getEquipamentoById(HttpServletRequest request) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			equipamento = new EquipamentoDao().getId(Integer.parseInt(id));
			request.setAttribute("equipamento", equipamento);
			// request.setAttribute("produtoId", Integer.parseInt(id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void exportExcel(HttpServletResponse response) throws Exception {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=relatorioEquipamentos.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Equipamentos");
		List<EquipamentoModel> equipamentos = new EquipamentoDao().getAll();

		sheet.setDefaultColumnWidth(20);
		sheet.setDefaultRowHeight((short) 400);
		sheet.setHorizontallyCenter(true);

		int rowNo = 0;
		Row row = sheet.createRow(rowNo++);
		int cellnum = 0;

		Cell cell = row.createCell(cellnum);
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("RELATORIO_EQUIPAMENTOS");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Sistema Operacional");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Tipo");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Arquitetura");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Processador");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Memoria");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Modelo");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Localidade");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Setor");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Nome Computador");
		
		cell = row.createCell(cellnum++);
		cell.setCellValue("Usuario Resp");

		for (EquipamentoModel e : equipamentos) {
			cellnum = 0;
			row = sheet.createRow(rowNo++);

			cell = row.createCell(cellnum++);
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getTipoEquipamento());

			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getSistemaOperacional());

			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getArquitetura());

			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getProcessador());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getMemoria());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getMarca());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getFilial());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getSetor());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getNomeComp());
			
			cell = row.createCell(cellnum++);
			cell.setCellValue(e.getUsuarioResp());
		}

		wb.write(response.getOutputStream());
		wb.close();

	}
}