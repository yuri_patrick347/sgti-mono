package com.mon.sgti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mon.sgti.model.UsuarioModel;
import com.mon.sgti.util.Conexao;

public class UsuarioDao {
	private PreparedStatement ps;
	private Connection conexao;
	private ResultSet rs;

	public void adiciona(UsuarioModel usuario) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "insert into tb_usuario (ds_nomUsuario, ds_email, ds_login, ds_senha)"
					+ "values (?,?,?,?)";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, usuario.getNom_Usuario());
			ps.setString(2, usuario.getEmail());
			ps.setString(3, usuario.getUsuario());
			ps.setString(4, usuario.getSenha());
		
			ps.executeUpdate();

		} catch (Exception e) {
			System.out
					.println("Erro ao salvar o usuario : " + e.getMessage() + "\n" + "Causa do erro :" + e.getCause());
		} finally {
			conexao.close();
		}

	}

	public void atualizar(UsuarioModel usuario) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "UPDATE  tb_usuario SET" + " ds_nomUsuario=?," + " ds_email=?," + " ds_login =?, "+  " ds_senha =? " 
					+ " WHERE id_usuario =?";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, usuario.getNom_Usuario());
			ps.setString(2, usuario.getEmail());
			ps.setString(3, usuario.getUsuario());
			ps.setString(4, usuario.getSenha());
			ps.setInt(5, usuario.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro n�o foi possivel atualizar o usuario : " + e.getMessage());
		} finally {
			conexao.close();
		}
	}

	public void excluir(UsuarioModel Usuario) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "DELETE FROM tb_Usuario WHERE id_usuario=?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, Usuario.getId());

			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro ao deletar o usuario: " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
	}

	public List<UsuarioModel> getAll() throws Exception {
		List<UsuarioModel> lista = new ArrayList<>();
		try {
			conexao = Conexao.getConnection();
			String sql = "select * from tb_usuario";
			ps = conexao.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				UsuarioModel Usuario = new UsuarioModel();
				Usuario.setId(rs.getInt("id_Usuario"));
				Usuario.setNom_Usuario(rs.getString("ds_nomUsuario"));
				Usuario.setEmail(rs.getString("ds_email"));
				Usuario.setUsuario(rs.getString("ds_login"));
				Usuario.setSenha(rs.getString("ds_senha"));
				lista.add(Usuario);
			}
		} catch (Exception e) {
			System.out.println(
					"Erro ao pesquisa lista usuario, com descricao do usuario " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
		return lista;
	}

	public UsuarioModel getCodigo(int id) throws Exception {
		UsuarioModel Usuario = null;
		try {
			conexao = Conexao.getConnection();
			String sql = "SELECT * FROM tb_usuario WHERE id_usuario =?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Usuario = new UsuarioModel();
				Usuario.setId(rs.getInt("id_usuario"));
				Usuario.setNom_Usuario(rs.getString("ds_nomUsuario"));
				Usuario.setEmail(rs.getString("ds_email"));
				Usuario.setUsuario(rs.getString("ds_login"));
				Usuario.setSenha(rs.getString("ds_senha"));
			}

		} catch (Exception e) {
			System.out.println("Erro ao pesquisa Usuario " + "\n" + e.getMessage());
		} finally {
			conexao.close();
		}
		return Usuario;
	}

}
