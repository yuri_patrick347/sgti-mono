package com.mon.sgti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mon.sgti.model.UsuarioModel;
import com.mon.sgti.util.Conexao;

public class LoginDao {
	
	private PreparedStatement ps;
	private ResultSet rs;
	private Connection conexao;
	
	public UsuarioModel logar(UsuarioModel login)throws Exception{
		String sql ="select * from tb_usuario where ds_login =? and ds_senha =?";
		UsuarioModel login2 =null;
		try {
			conexao = Conexao.getConnection();
			ps = conexao.prepareStatement(sql);
			ps.setString(1, login.getUsuario());
			ps.setString(2, login.getSenha());
			rs = ps.executeQuery();
			while (rs.next()){
				login2 = new UsuarioModel();
				login2.setUsuario(rs.getString("ds_login"));
				login2.setSenha(rs.getString("ds_senha"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return login2;
	}

}
