package com.mon.sgti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mon.sgti.model.ProdutoModel;
import com.mon.sgti.util.Conexao;

public class ProdutoDao {
	private PreparedStatement ps;
	private Connection conexao;
	private ResultSet rs;

	public void adiciona(ProdutoModel produto) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "insert into tb_produto (desc_produto,quant_produto,refer_produto,mod_produto,forn_produto,dataComp_produto)"
					+ "values (?,?,?,?,?,?)";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, produto.getDescricao());
			ps.setDouble(2, produto.getQuantidade());
			ps.setInt(3, produto.getReferencia());
			ps.setString(4, produto.getModelo());
			ps.setString(5, produto.getFornecedor());
			ps.setString(6, produto.getDataCompra());
			ps.executeUpdate();

		} catch (Exception e) {
			System.out
					.println("Erro ao salvar o produto : " + e.getMessage() + "\n" + "Causa do erro :" + e.getCause());
		} finally {
			conexao.close();
		}

	}

	public void atualizar(ProdutoModel produto) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "UPDATE  tb_produto SET" + " desc_produto=?," + " quant_produto=?," + " refer_produto =?,"
					+ " mod_produto =?," + " forn_produto =?," + " dataComp_produto =?" + " WHERE id_produto =?";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, produto.getDescricao());
			ps.setDouble(2, produto.getQuantidade());
			ps.setInt(3, produto.getReferencia());
			ps.setString(4, produto.getModelo());
			ps.setString(5, produto.getFornecedor());
			ps.setString(6, produto.getDataCompra());
			ps.setInt(7, produto.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro n�o foi possivel atualizar o produto : " + e.getMessage());
		} finally {
			conexao.close();
		}
	}

	public void excluir(ProdutoModel produto) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "DELETE FROM tb_produto WHERE id_produto=?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, produto.getId());

			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro ao deletar o produto: " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
	}

	public List<ProdutoModel> getAll() throws Exception {
		List<ProdutoModel> lista = new ArrayList<>();
		try {
			conexao = Conexao.getConnection();
			String sql = "select * from tb_produto";
			ps = conexao.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ProdutoModel produto = new ProdutoModel();
				produto.setId(rs.getInt("id_produto"));
				produto.setDescricao(rs.getString("desc_produto"));
				produto.setQuantidade(rs.getInt("quant_produto"));
				produto.setReferencia(rs.getInt("refer_produto"));
				produto.setModelo(rs.getString("mod_produto"));
				produto.setFornecedor(rs.getString("forn_produto"));
				produto.setDataCompra(rs.getString("dataComp_produto"));
				lista.add(produto);
			}
		} catch (Exception e) {
			System.out.println(
					"Erro ao pesquisa lista produto, com descricao do produto " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
		return lista;
	}

	public ProdutoModel getCodigo(int id) throws Exception {
		ProdutoModel produto = null;
		try {
			conexao = Conexao.getConnection();
			String sql = "SELECT * FROM tb_produto WHERE id_produto =?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				produto = new ProdutoModel();
				produto.setId(rs.getInt("id_produto"));
				produto.setDescricao(rs.getString("desc_produto"));
				produto.setQuantidade(rs.getInt("quant_produto"));
				produto.setReferencia(rs.getInt("refer_produto"));
				produto.setModelo(rs.getString("mod_produto"));
				produto.setFornecedor(rs.getString("forn_produto"));
				produto.setDataCompra(rs.getString("dataComp_produto"));
			}

		} catch (Exception e) {
			System.out.println("Erro ao pesquisa produto " + "\n" + e.getMessage());
		} finally {
			conexao.close();
		}
		return produto;
	}

}
