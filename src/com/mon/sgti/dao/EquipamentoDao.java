package com.mon.sgti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mon.sgti.model.EquipamentoModel;
import com.mon.sgti.util.Conexao;

public class EquipamentoDao {
	private PreparedStatement ps;
	private Connection conexao;
	private ResultSet rs;

	public void adiciona(EquipamentoModel equipamento) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "insert into tb_equipamento (tipo_equipamento,so_equipamento,arq_equipamento,proc_equipamento,memor_equipamento,marca_equipamento,"
					+ "filial_equipamento,setor_equipamento,nomeComp_equipamento,usuarioResp_equipamento) values (?,?,?,?,?,?,?,?,?,?)";
			
			ps = conexao.prepareStatement(sql);
			ps.setString(1, equipamento.getTipoEquipamento());
			ps.setString(2, equipamento.getSistemaOperacional());
			ps.setInt(3, equipamento.getArquitetura());
			ps.setString(4, equipamento.getProcessador());
			ps.setInt(5, equipamento.getMemoria());
			ps.setString(6, equipamento.getMarca());
			ps.setString(7, equipamento.getFilial());
			ps.setString(8, equipamento.getSetor());
			ps.setString(9, equipamento.getNomeComp());
			ps.setString(10, equipamento.getUsuarioResp());
			ps.executeUpdate();

		} catch (Exception e) {
			System.out
					.println("Erro ao salvar o equipamento : " + e.getMessage() + "\n" + "Causa do erro :" + e.getCause());
		} finally {
			conexao.close();
		}

	}

	public void atualizar(EquipamentoModel equipamento) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "UPDATE  tb_equipamento SET" + " tipo_Equipamento=?," + " so_Equipamento=?," + " arq_Equipamento =?,"  
					+ " proc_Equipamento =?," + " memor_Equipamento =?," + " marca_Equipamento =?," + " filial_Equipamento =?,"  
					+ " setor_Equipamento =?," + " nomeComp_Equipamento =?," + " usuarioResp_Equipamento =?" + " WHERE id_equipamento =?";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, equipamento.getTipoEquipamento());
			ps.setString(2, equipamento.getSistemaOperacional());
			ps.setInt(3, equipamento.getArquitetura());
			ps.setString(4, equipamento.getProcessador());
			ps.setInt(5, equipamento.getMemoria());
			ps.setString(6, equipamento.getMarca());
			ps.setString(7, equipamento.getFilial());
			ps.setString(8, equipamento.getSetor());
			ps.setString(9, equipamento.getNomeComp());
			ps.setString(10, equipamento.getUsuarioResp());
			ps.setInt(11, equipamento.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro n�o foi possivel atualizar o equipamento : " + e.getMessage());
		} finally {
			conexao.close();
		}
	}

	public void excluir(EquipamentoModel equipamento) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "DELETE FROM tb_equipamento WHERE id_equipamento=?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, equipamento.getId());

			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro ao deletar o equipamento: " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
	}

	public List<EquipamentoModel> getAll() throws Exception {
		List<EquipamentoModel> lista = new ArrayList<>();
		try {
			conexao = Conexao.getConnection();
			String sql = "select * from tb_equipamento";
			ps = conexao.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				EquipamentoModel equipamento = new EquipamentoModel();
				equipamento.setId(rs.getInt("id_equipamento"));
				equipamento.setTipoEquipamento(rs.getString("tipo_Equipamento"));
				equipamento.setSistemaOperacional(rs.getString("so_Equipamento"));
				equipamento.setArquitetura(rs.getInt("arq_Equipamento"));
				equipamento.setProcessador(rs.getString("proc_Equipamento"));
				equipamento.setMemoria(rs.getInt("memor_Equipamento"));
				equipamento.setMarca(rs.getString("marca_Equipamento"));
				equipamento.setFilial(rs.getString("filial_Equipamento"));
				equipamento.setSetor(rs.getString("setor_Equipamento"));
				equipamento.setNomeComp(rs.getString("nomeComp_Equipamento"));
				equipamento.setUsuarioResp(rs.getString("usuarioResp_Equipamento"));
				lista.add(equipamento);
			}
		} catch (Exception e) {
			System.out.println(
					"Erro ao pesquisa lista equipamento, com descricao do equipamento " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
		return lista;
	}

	public EquipamentoModel getId(int id) throws Exception {
		EquipamentoModel equipamento = null;
		try {
			conexao = Conexao.getConnection();
			String sql = "SELECT * FROM tb_equipamento WHERE id_equipamento =?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				equipamento = new EquipamentoModel();
				equipamento.setId(rs.getInt("id_equipamento"));
				equipamento.setTipoEquipamento(rs.getString("tipo_Equipamento"));
				equipamento.setSistemaOperacional(rs.getString("so_Equipamento"));
				equipamento.setArquitetura(rs.getInt("arq_Equipamento"));
				equipamento.setProcessador(rs.getString("proc_Equipamento"));
				equipamento.setMemoria(rs.getInt("memor_Equipamento"));
				equipamento.setMarca(rs.getString("marca_Equipamento"));
				equipamento.setFilial(rs.getString("filial_Equipamento"));
				equipamento.setSetor(rs.getString("setor_Equipamento"));
				equipamento.setNomeComp(rs.getString("nomeComp_Equipamento"));
				equipamento.setUsuarioResp(rs.getString("usuarioResp_Equipamento"));
			}

		} catch (Exception e) {
			System.out.println("Erro ao pesquisa equipamento " + "\n" + e.getMessage());
		} finally {
			conexao.close();
		}
		return equipamento;
	}

}
