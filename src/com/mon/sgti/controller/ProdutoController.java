package com.mon.sgti.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mon.sgti.service.ProdutoService;

@WebServlet({ "/novo-produto", "/salva-produto", "/atualiza-produto", "/exclui-produto", "/lista-produtos",
		"/getId-produto", "/rel-produtos" })
public class ProdutoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProdutoService produtoService = new ProdutoService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			executa(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void executa(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String path = request.getContextPath();
		String uri = request.getRequestURI();

		// NOVO PRODUTO
		if (uri.equalsIgnoreCase(path + "/novo-produto")) {
			request.getRequestDispatcher("/novo-produto.jsp").forward(request, response);
		}

		// SALVAR PRODUTO
		if (uri.equalsIgnoreCase(path + "/salva-produto")) {
			produtoService.adiciona(request);
			response.sendRedirect(path + "/novo-produto");
		}

		// LISTAR TODOS OS PRODUTOS
		if (uri.equalsIgnoreCase(path + "/lista-produtos")) {
			produtoService.getAll(request);
			request.getRequestDispatcher("/lista-produtos.jsp").forward(request, response);
		}

		// BUSCA PRODUTO PELO ID
		if (uri.equalsIgnoreCase(path + "/getId-produto")) {
			produtoService.getProdutoById(request);
			request.getRequestDispatcher("/atualiza-produto.jsp").forward(request, response);
		}

		// ATUALIZA O PRODUTO
		if (uri.equalsIgnoreCase(path + "/atualiza-produto")) {
			produtoService.atualiza(request);
			response.sendRedirect(path + "/lista-produtos");
		}

		// EXCLUI OS PRODUTOS
		if (uri.equalsIgnoreCase(path + "/exclui-produto")) {
			produtoService.exclui(request);
			response.sendRedirect(path + "/lista-produtos");
		}

		// EXPORTAR EXCEL
		if (uri.equalsIgnoreCase(path + "/rel-produtos")) {
			produtoService.exportExcel(response);
		}

	}
}