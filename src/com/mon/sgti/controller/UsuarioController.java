package com.mon.sgti.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mon.sgti.service.UsuarioService;

@WebServlet({ "/novo-usuario", "/salva-usuario", "/atualiza-usuario", "/exclui-usuario", "/consulta-usuarios",
		"/getId-usuario" })
public class UsuarioController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UsuarioService UsuarioService = new UsuarioService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			executa(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void executa(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String path = request.getContextPath();
		String uri = request.getRequestURI();

		// NOVO USUARIO
		if (uri.equalsIgnoreCase(path + "/novo-usuario")) {
			request.getRequestDispatcher("/novo-usuario.jsp").forward(request, response);
		}

		// SALVAR USUARIO
		if (uri.equalsIgnoreCase(path + "/salva-usuario")) {
			UsuarioService.adiciona(request);
			response.sendRedirect(path + "/novo-usuario");
		}

		// LISTAR TODOS OS USUARIO
		if (uri.equalsIgnoreCase(path + "/consulta-usuarios")) {
			UsuarioService.getAll(request);
			request.getRequestDispatcher("/consulta-usuarios.jsp").forward(request, response);
		}

		// BUSCA USUARIO PELO ID
		if (uri.equalsIgnoreCase(path + "/getId-usuario")) {
			UsuarioService.getUsuarioById(request);
			request.getRequestDispatcher("/atualiza-usuario.jsp").forward(request, response);
		}

		// ATUALIZA O USUARIO
		if (uri.equalsIgnoreCase(path + "/atualiza-usuario")) {
			UsuarioService.atualiza(request);
			response.sendRedirect(path + "/consulta-usuarios");
		}

		// EXCLUI OS USUARIO
		if (uri.equalsIgnoreCase(path + "/exclui-usuario")) {
			UsuarioService.exclui(request);
			response.sendRedirect(path + "/consulta-usuarios");
		}

	}
}