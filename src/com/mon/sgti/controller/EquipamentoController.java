package com.mon.sgti.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mon.sgti.service.EquipamentoService;

@WebServlet({ "/novo-equipamento", "/salva-equipamento", "/atualiza-equipamento", "/exclui-equipamento",
		"/lista-equipamentos", "/getId-equipamento", "/rel-equipamentos" })
public class EquipamentoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private EquipamentoService equipamentoService = new EquipamentoService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			executa(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void executa(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String path = request.getContextPath();
		String uri = request.getRequestURI();

		// NOVO EQUIPAMENTO
		if (uri.equalsIgnoreCase(path + "/novo-equipamento")) {
			request.getRequestDispatcher("/novo-equipamento.jsp").forward(request, response);
		}

		// SALVAR EQUIPAMENTO
		if (uri.equalsIgnoreCase(path + "/salva-equipamento")) {
			equipamentoService.adiciona(request);
			response.sendRedirect(path + "/novo-equipamento");
		}

		// LISTAR TODOS OS EQUIPAMENTO
		if (uri.equalsIgnoreCase(path + "/lista-equipamentos")) {
			equipamentoService.getAll(request);
			request.getRequestDispatcher("/lista-equipamentos.jsp").forward(request, response);
		}

		// BUSCA EQUIPAMENTO PELO ID
		if (uri.equalsIgnoreCase(path + "/getId-equipamento")) {
			equipamentoService.getEquipamentoById(request);
			request.getRequestDispatcher("/atualiza-equipamento.jsp").forward(request, response);
		}

		// ATUALIZA O EQUIPAMENTO
		if (uri.equalsIgnoreCase(path + "/atualiza-equipamento")) {
			equipamentoService.atualiza(request);
			response.sendRedirect(path + "/lista-equipamentos");
		}

		// EXCLUI OS EQUIPAMENTO
		if (uri.equalsIgnoreCase(path + "/exclui-equipamento")) {
			equipamentoService.exclui(request);
			response.sendRedirect(path + "/lista-equipamentos");
		}

		// EXPORTAR EXCEL
		if (uri.equalsIgnoreCase(path + "/rel-equipamentos")) {
			equipamentoService.exportExcel(response);
		}

	}
}