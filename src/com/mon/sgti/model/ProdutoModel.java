package com.mon.sgti.model;

public class ProdutoModel {

	private Integer id;

	private String descricao;

	private Integer quantidade;

	private Integer referencia;

	private String modelo;

	private String fornecedor;

	private String dataCompra;
	
	public ProdutoModel() {
	}

	public ProdutoModel(Integer id, String descricao, Integer quantidade, Integer referencia, String modelo,
			String fornecedor, String dataCompra) {
		this.id = id;
		this.descricao = descricao;
		this.quantidade = quantidade;
		this.referencia = referencia;
		this.modelo = modelo;
		this.fornecedor = fornecedor;
		this.dataCompra = dataCompra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getReferencia() {
		return referencia;
	}

	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}

}
