package com.mon.sgti.model;


public class UsuarioModel {

	private int id;
	private String usuario;
	private String senha;
	private String nom_Usuario;
	private String email;
	
	public UsuarioModel() {
	}

	public UsuarioModel(int id, String usuario, String senha, String nom_Usuario, String email) {
		this.id = id;
		this.usuario = usuario;
		this.senha = senha;
		this.nom_Usuario = nom_Usuario;
		this.email = email;
	}
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNom_Usuario() {
		return nom_Usuario;
	}
	public void setNom_Usuario(String nom_Usuario) {
		this.nom_Usuario = nom_Usuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}