<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/site.css">
<style type="text/css">
.div-externa-update-usuario {
	position: absolute;
	width: 490px;
	height: 440px;
	border-radius: 5px;
	background-color: #E8E8E8;
	padding: 20px;
	border-radius: 20px;
	margin: 6% -50%;
}

.div-interna-update-usuario {
	position: absolute;
	width: 450px;
	height: 360px;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 15px;
}
</style>

<title>Atualiza usuario</title>


</head>
<body>
	<jsp:include page="home.jsp" />
	<form action="atualiza-usuario" name="dados" method="post">

		<div class="div-externa-update-usuario">
			<p class="p-update-usuario">Atualização do usuario</p>
			<div class="div-interna-update-usuario">
				<table>
					<tr>
						<td>ID</td>
						<td><input type="text" class="input-update-usuario"
							readonly="readonly" name="id_User" value="${usuario.id}"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Nome</td>
						<td><input type="email" class="input-update-usuario"
							name="nome_User" value="${usuario.nom_Usuario}" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>E-mail</td>
						<td><input type="email" class="input-update-usuario"
							name="email_User" value="${usuario.email}" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Usuario</td>
						<td><input type="text" class="input-update-usuario"
							name="login_User" value="${usuario.usuario}" required="required"
							style="width: 315px"></td>
					</tr>

					<tr>
						<td>Senha</td>
						<td><input type="password" class="input-update-usuario"
							name="senha_User" value="${usuario.senha}" required="required"
							style="width: 315px"></td>
					</tr>
					
				</table>
				<input type="submit" class="btn-update-usuario" id="gravar"
					value="Gravar">
			</div>
		</div>
	</form>
</body>
</html>