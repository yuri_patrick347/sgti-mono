<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/site.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<style type="text/css">
.div-externa-novo-usuario {
	position: absolute;
	width: 490px;
	height: 400px;
	border-radius: 5px;
	background-color: #E8E8E8;
	padding: 20px;
	border-radius: 20px;
	margin: 6% -50%;
}

.div-interna-novo-usuario {
	position: absolute;
	width: 450px;
	height: 320px;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 15px;
}
</style>

<title>Cadastro de Usuario</title>

</head>
<body>
	<jsp:include page="home.jsp" />
	<form action="salva-usuario" name="dados" method="post">
		<div class="div-externa-novo-usuario">
			<p class="p-novo-usuario">Cadastro de Usuario</p>
			<div class="div-interna-novo-usuario">
				<table>
					<tr>
						<td>Nome usuario</td>
						<td><input type="text" class="input-novo-usuario"
							id="nome_User" name="nome_User" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>E-Mail</td>
						<td><input type="email" class="input-novo-usuario"
							id="email_User" name="email_User" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Usuario</td>
						<td><input type="text" class="input-novo-usuario"
							id="login_User" name="login_User" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Senha</td>
						<td><input type="password" class="input-novo-usuario"
							id="senha_User" name="senha_User" required="required"
							style="width: 315px"></td>
					</tr>

				</table>

				<input type="submit" class="btn-novo-usuario" id="gravar"
					value="Gravar"> <input type="reset"
					class="btn-novo-usuario" id="cancelar" value="Cancelar">
			</div>
		</div>
	</form>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>

</body>
</html>