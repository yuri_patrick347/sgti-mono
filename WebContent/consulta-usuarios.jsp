<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/consLog.css">
	<style type="text/css">
		.div-lista {
			position: absolute;
			width: 80%;
			height:300px;
			border-radius: 5px;
			background-color: #f2f2f2;
			padding: 20px;
			border-radius: 20px;
			margin-top: 30%;
		}
	</style>
	
<title>Lista Usuario</title>
	
</head>
<body>
<jsp:include page="home.jsp" />
	<div class="div-lista">
		<section>
			<div class="tbl-header">
				<table>
					<thead>
						<tr>
							<th>ID</th>
							<th>Nome</th>
							<th>E-mail</th>
							<th>Usuario</th>
							<th style="width: 140px">Opera��es</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="tbl-content">
				<table>
					<tbody>
						<c:forEach var="usuario" items="${usuarios}">
							<tr class="header">
								<td>${usuario.id}</td>
								<td>${usuario.nom_Usuario}</td>
								<td>${usuario.email}</td>
								<td>${usuario.usuario}</td>
								
								<td style="width: 70px"><a href="<c:url value="/getId-usuario?id=${usuario.id}"/>">Alterar</a></td>
	
								<td style="width: 70px"><a href="<c:url value="/exclui-usuario?id=${usuario.id}"/>"
									onclick="return confirm('Deseja realmente excluir o usuario')" id="ok">Excluir</a></td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</section>

	</div>
</body>
</html>