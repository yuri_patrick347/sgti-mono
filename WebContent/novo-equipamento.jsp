<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/site.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<style type="text/css">
.div-externa-novo-equipamento {
	position: absolute;
	width: 930px;
	height: 480px;
	border-radius: 5px;
	background-color: #E8E8E8;
	padding: 20px;
	border-radius: 20px;
	margin: 6% -66%;
}

.div-interna-novo-equipamento {
	position: absolute;
	width: 895px;
	height: 400px;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 15px;
}
</style>

<title>Cadastro Equipamento</title>

</head>
<body>
	<jsp:include page="home.jsp" />
	
	<form action="salva-equipamento" name="dados" method="post">
		<div class="div-externa-novo-equipamento">
			<p class="p-novo-equipamento">Cadastro de Equipamento</p>
			<div class="div-interna-novo-equipamento">
				<table>
					<tr>
						<td>Tipo Equipamento</td>
						<td><input type="text" class="input-novo-equipamento"
							id="tipo_Equip" name="tipo_Equip" required="required"
							style="width: 315px"></td>
					
						<td>Sistema Operacional</td>
						<td><input type="text" class="input-novo-equipamento"
							id="sistema_Equip" name="sistema_Equip" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Arquitatura</td>
						<td><input type="number" class="input-novo-equipamento"
							id="arq_Equip" name="arq_Equip" value="0" required="required"
							style="width: 315px"></td>
					
						<td>Processador</td>
						<td><input type="text" class="input-novo-equipamento"
							id="processador_Equip" name="processador_Equip" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Memoria</td>
						<td><input type="number" class="input-novo-equipamento"
							id="memoria_Equip" name="memoria_Equip" value="0" required="required"
							style="width: 315px"></td>
					
						<td>Modelo</td>
						<td><input type="text" class="input-novo-equipamento"
							id="marca_Equip" name="marca_Equip" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
					
						<td>Localiza��o Equipamento</td>
						<td><input type="text" class="input-novo-equipamento"
							id="filial_Equip" name="filial_Equip" required="required"
							style="width: 315px"></td>
					
						<td>Setor Equipamento</td>
						<td><input type="text" class="input-novo-equipamento"
							id="setor_Equip" name="setor_Equip" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
				
						<td>Nome Equipamento</td>
						<td><input type="text" class="input-novo-equipamento"
							id="nom_comp_Equip" name="nom_comp_Equip" required="required"
							style="width: 315px"></td>
					
						<td>Nome do Us�rio Responsavel</td>
						<td><input type="text" class="input-novo-equipamento"
							id="usua_resp_Equip" name="usua_resp_Equip" required="required"
							style="width: 315px"></td>
					</tr>
					
				</table>


				<input type="submit" class="btn-novo-equipamento" id="gravar"
					value="Gravar"> <input type="reset"
					class="btn-novo-equipamento" id="cancelar" value="Cancelar">

			</div>

		</div>

	</form>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>

</body>
</html>