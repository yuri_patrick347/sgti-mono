<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/site.css">
<style type="text/css">
.div-externa-update-produto {
	position: absolute;
	width: 490px;
	height: 540px;
	border-radius: 5px;
	background-color: #E8E8E8;
	padding: 20px;
	border-radius: 20px;
	margin: 6% -50%;
}

.div-interna-update-produto {
	position: absolute;
	width: 450px;
	height: 465px;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 15px;
}
</style>

<title>Atualiza Produto</title>


</head>
<body>
	<jsp:include page="home.jsp" />
	<form action="atualiza-produto" name="dados" method="post">

		<div class="div-externa-update-produto">
			<p class="p-update-produto">Atualização do Produto</p>
			<div class="div-interna-update-produto">
				<table>
					<tr>
						<td>ID</td>
						<td><input type="text" class="input-update-produto"
							readonly="readonly" name="id_Prod" value="${produto.id}"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Descrição</td>
						<td><input type="text" class="input-update-produto"
							name="desc_Prod" value="${produto.descricao}" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Quantidade</td>
						<td><input type="number" class="input-update-produto"
							name="qnt_Prod" value="${produto.quantidade}" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Referencia</td>
						<td><input type="number" class="input-update-produto"
							name="ref_Prod" value="${produto.referencia}" required="required"
							style="width: 315px"></td>
					</tr>

					<tr>
						<td>Modelo</td>
						<td><input type="text" class="input-update-produto"
							name="mod_Prod" value="${produto.modelo}" required="required"
							style="width: 315px"></td>
					</tr>
					
					<tr>
						<td>Fornecedor</td>
						<td><input type="text" class="input-update-produto"
							name="forn_Prod" value="${produto.fornecedor}" required="required"
							style="width: 315px"></td>
					</tr>
					
					<tr>
						<td>Data da Compra</td>
						<td><input type="date" class="input-update-produto"
							name="dat_Comp_Prod" value="${produto.dataCompra}" required="required"
							style="width: 315px"></td>
					</tr>
				</table>
				<input type="submit" class="btn-update-produto" id="gravar"
					value="Gravar">
			</div>
		</div>
	</form>
</body>
</html>