<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/consEquip.css">
	<style type="text/css">
		.div-lista {
			position: absolute;
			width: 100%;
			height:300px;
			border-radius: 5px;
			background-color: #f2f2f2;
			padding: 20px;
			border-radius: 20px;
			margin-top: 30%;
		}
	</style>
	
<title>Lista Equipamento</title>
	
</head>
<body>
<jsp:include page="home.jsp" />
	<div class="div-lista">
		<section>
			<div class="tbl-header">
				<table>
					<thead>
						<tr>
							<th>ID</th>
							<th>Tipo do Equipamento</th>
							<th>Sistema Operacional</th>
							<th>Arquitetura</th>
							<th>Processador</th>
							<th>Memoria</th>
							<th>Modelo</th>
							<th>Localidade</th>
							<th>Setor</th>
							<th>Nome Computador</th>
							<th>Usu�rio Responsavel</th>
							<th style="width: 110px">Opera��es</th>
							
						</tr>
					</thead>
				</table>
			</div>
			<div class="tbl-content">
				<table>
					<tbody>
						<c:forEach var="equipamento" items="${equipamentos}">
							<tr class="header">
								<td>${equipamento.id}</td>
								<td>${equipamento.tipoEquipamento}</td>
								<td>${equipamento.sistemaOperacional}</td>
								<td>${equipamento.arquitetura}</td>
								<td>${equipamento.processador}</td>
								<td>${equipamento.memoria}</td>
								<td>${equipamento.marca}</td>
								<td>${equipamento.filial}</td>
								<td>${equipamento.setor}</td>
								<td>${equipamento.nomeComp}</td>
								<td>${equipamento.usuarioResp}</td>
								
								<td style="width: 55px"><a href="<c:url value="/getId-equipamento?id=${equipamento.id}"/>">Alterar</a></td>
	
								<td style="width: 55px"><a href="<c:url value="/exclui-equipamento?id=${equipamento.id}"/>"
									onclick="return confirm('Deseja realmente excluir o Produto')" id="ok">Excluir</a></td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<form action="rel-equipamentos" name="dados" method="post">
					<input style="margin-top: 1%; margin-left: 90%" 
					type="submit" id="exportar" class="btn-exp-equipamento" value="Exportar Dados">
				</form>
				
			</div>
			
		</section>

	</div>
</body>
</html>