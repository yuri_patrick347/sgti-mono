<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/consProd.css">
	<style type="text/css">
		.div-lista {
			position: absolute;
			width: 80%;
			height:300px;
			border-radius: 5px;
			background-color: #f2f2f2;
			padding: 20px;
			border-radius: 20px;
			margin-top: 30%;
		}
	</style>
	
<title>Lista Produto</title>
	
</head>
<body>
<jsp:include page="home.jsp" />
	<div class="div-lista">
		<section>
			<div class="tbl-header">
				<table>
					<thead>
						<tr>
							<th>ID</th>
							<th>Descri��o</th>
							<th>Quantidade</th>
							<th>Referencia</th>
							<th>Modelo</th>
							<th>Fornecedor</th>
							<th>Data da Compra</th>
							<th style="width: 140px">Opera��es</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="tbl-content">
				<table>
					<tbody>
						<c:forEach var="produto" items="${produtos}">
							<tr class="header">
								<td>${produto.id}</td>
								<td>${produto.descricao}</td>
								<td>${produto.quantidade}</td>
								<td>${produto.referencia}</td>
								<td>${produto.modelo}</td>
								<td>${produto.fornecedor}</td>
								<td>${produto.dataCompra}</td>
								
								<td style="width: 70px"><a href="<c:url value="/getId-produto?id=${produto.id}"/>">Alterar</a></td>
	
								<td style="width: 70px"><a href="<c:url value="/exclui-produto?id=${produto.id}"/>"
									onclick="return confirm('Deseja realmente excluir o Produto')" id="ok">Excluir</a></td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<form action="rel-produtos" name="dados" method="post">
					<input style="margin-top: 1%; margin-left: 87%"
					 type="submit" id="exportar" class="btn-novo-produto" value="Exportar Dados">
				</form>
				
			</div>
			
		</section>
		
	</div>
	
</body>
</html>