<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/site.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<style type="text/css">
.div-externa-novo-produto {
	position: absolute;
	width: 490px;
	height: 490px;
	border-radius: 5px;
	background-color: #E8E8E8;
	padding: 20px;
	border-radius: 20px;
	margin: 6% -50%;
}

.div-interna-novo-produto {
	position: absolute;
	width: 450px;
	height: 420px;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 15px;
}
</style>

<title>Cadastro Produto</title>

</head>
<body>
	<jsp:include page="home.jsp" />
	<form action="salva-produto" name="dados" method="post">
		<div class="div-externa-novo-produto">
			<p class="p-novo-produto">Cadastro de Produtos</p>
			<div class="div-interna-novo-produto">
				<table>
					<tr>
						<td>Descri��o</td>
						<td><input type="text" class="input-novo-produto"
							id="desc_Prod" name="desc_Prod" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Quantidade</td>
						<td><input type="number" class="input-novo-produto"
							id="qnt_Prod" name="qnt_Prod" value="0" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Referencia</td>
						<td><input type="number" class="input-novo-produto"
							id="ref_Prod" name="ref_Prod" value="0" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Modelo</td>
						<td><input type="text" class="input-novo-produto"
							id="mod_Prod" name="mod_Prod" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>fornecedor</td>
						<td><input type="text" class="input-novo-produto"
							id="forn_Prod" name="forn_Prod" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Data da Compra</td>
						<td><input type="date" class="input-novo-produto"
							id="dat_Comp_Prod" name="dat_Comp_Prod" required="required"
							style="width: 315px"></td>
					</tr>
				</table>


				<input type="submit" class="btn-novo-produto" id="gravar"
					value="Gravar"> <input type="reset"
					class="btn-novo-produto" id="cancelar" value="Cancelar">

			</div>

		</div>

	</form>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>

</body>
</html>