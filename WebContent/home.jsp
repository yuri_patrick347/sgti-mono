<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
	<link rel="stylesheet" type="text/css" href="css/home.css">
<!------ Include the above in your HEAD tag ---------->
<title>Home</title>
</head>
<body>

<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link" href="./novo-equipamento">Novo Equipamento</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="./lista-equipamentos">Lista Equipamento</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="./novo-produto">Novo Produto</a>
  </li>
   <li class="nav-item">
    <a class="nav-link" href="./lista-produtos">Lista Produto</a>
  </li>
    <li class="nav-item">
    <a class="nav-link" href="./novo-usuario">Novo Usuário</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="./consulta-usuarios">Consulta Usuário</a>
  </li>
   <li class="nav-item">
    <a class="nav-link" href="./logof">Sair</a>
  </li>
</ul>
      
</body>
</html>