# 🖥 Sistema de Gerenciamento de Equipamentos de TI

## 📄 Descrição do Projeto

<p align="center">O projeto consiste em um sistema que gerencia os equipamentos de TI, controlando os equipamentos e os produtos em estoque. As funções principais do sistema são cadastrar, consultar, atualizar, excluir e com as operações de exportar dados em Excel.</p>

### 🔖 Requisitos Funcionais:

* O sistema terá cadastro de usuários;
* O sistema terá cadastro de equipamento;
* O sistema terá cadastro de produto;
* O sistema terá consulta dos equipamentos;
* O sistema emitirá um relatório da consulta dos equipamentos;
* O sistema terá consulta dos produtos em estoque;
* O sistema emitirá um relatório dos produtos em estoque;
* O sistema terá a consulta de perfil do usuário logado;
* O sistema disponibilizará a opção de alterar produto em estoque e remover;
* O sistema disponibilizará a opção de alterar equipamento em estoque e remover;



## 📄 Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Java SE Development Kit 11](https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html), [Eclipse-IDE](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2021-06/R/eclipse-inst-jre-win64.exe), [Apache-Tomcat](https://tomcat.apache.org/download-90.cgi), [MariaDB](https://mariadb.org/download/).

### 🎲 Rodando o Sistema:

* Clone este repositório;

* Instalar a IDE Eclipse;

* Iniciar o tomcat na IDE;

* Instalar o bando de dados MariaDB;

* Obs.: Será necessario importar na IDE as blibliotecas: JSP, MySQL Connector, Apache POI... Todas essas biblioteca estão no repositorio do projeto -> (https://gitlab.com/yuri_patrick347/sgti-mono/-/tree/master/WebContent/WEB-INF/lib);

## 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Java SE Development Kit 11](https://www.oracle.com/EN/java)
- [JDBC](https://www.mysql.com/products/connector/)
- [Apache POI](https://poi.apache.org/apidocs/index.html)
- [JSP](https://docs.oracle.com/javaee/5/tutorial/doc/bnajo.html)
- [Bootstrap](https://getbootstrap.com)

## 📄 Diagrama de Classe da Aplicação

Para o planejamento e o desenvolvimento do sistema, com os seus respectivos atributos, operações e métodos, foi desenvolvido o diagrama de classe, com as classes Equipamento, Usuário e Produto, enquanto as demais classes implementam ações de um sistema CRUD que são: cadastrar, consultar, atualizar e deletar. As classes de serviço de equipamento, produto e usuário são as que implementam as operações do CRUD.

![UML](src/com/mon/sgti/util/UML.png)
